# Apple I Emulator #
A CLI based emulator written in Rust. 
This project served as my first jump into emulator programming. While super educational it posed some challenges for me as i'm not yet accustomed to the patterns most effective for Rust.

## Usage ##
You can start the emulator with the following command

```cargo run```

The emulator will start in a paused state, and can be unpaused by pressing ```]```
The emulator can be paused or single stepped by pressing ```[```

## Compiling software ##
The roms folder contains multiple programs written in 6502 Assembly. These programs can be compiled using a program like `asm6`. 

- `ascii_test.s `A simple test program that prints all supported ascii characters.
- `opcode_test.s ` A program that validates multiple opcodes.
- `wozmon.s` A modified version of the original wozmon system monitor. Either it or the emulator has a bug preventing any valid output. Bonus points if you can find it.

## To-Do's
- Implement tests for each instruction validating its functionality.
