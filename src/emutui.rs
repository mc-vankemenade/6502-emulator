extern crate ncurses;
use ncurses::*;

pub struct Emutui {
    status_window: WINDOW,
    status_buffer: WINDOW,

    logging_window: WINDOW,
    logging_buffer: WINDOW,

    output_window: WINDOW,
    output_buffer: WINDOW,

    status_size: [i32; 2],
    output_size: [i32; 2],
}

impl Emutui {
    pub fn new() -> Emutui {

        initscr();
        cbreak();
        noecho();

        let mut scr_height: i32 = 0;
        let mut scr_width: i32 = 0;
        getmaxyx(stdscr(), &mut scr_height, &mut scr_width);

        let stat_size: [i32; 2] = [scr_height, (scr_width / 4)];
        let out_size: [i32; 2] = [scr_height, (scr_width / 4) * 3];

        let stat_win: WINDOW = newwin(stat_size[0], stat_size[1], 0, (scr_width / 4)*3);
        let stat_buf: WINDOW = derwin(stat_win, (stat_size[0] - 1) / 2, stat_size[1] - 2, 1, 1);
        let log_win: WINDOW = derwin(stat_win, (stat_size[0] - 1) / 2, stat_size[1] - 2, (stat_size[0] - 1) / 2, 1);
        let log_buf: WINDOW = derwin(log_win, (stat_size[0] - 2) / 2, stat_size[1] - 4, 1, 1);
        
        let out_win: WINDOW = newwin(out_size[0], out_size[1], 0, 0);
        let out_buf: WINDOW = derwin(out_win, out_size[0] - 2, out_size[1] - 2, 1, 1);
        
        scrollok(log_buf, true);
        scrollok(out_buf, true);
        wtimeout(out_buf, 0);
        keypad(out_buf, true);
        // nodelay(out_buf, true);

        box_(stat_win, 0, 0);
        box_(log_win, 0, 0);
        box_(out_win, 0, 0);
        
        mvwprintw(stat_win, 0, 0, "Status");
        mvwprintw(log_win, 0, 0, "Logging");
        mvwprintw(out_win, 0, 0, "Output");

        wrefresh(stat_win);
        wrefresh(out_win);
        wrefresh(log_win);

        return Emutui {
            status_window: stat_win,
            status_buffer: stat_buf,

            logging_window: log_win,
            logging_buffer: log_buf,

            output_window: out_win,
            output_buffer: out_buf,

            status_size: stat_size,
            output_size: out_size
        }
    }

    pub fn update_status(&self, status: Vec<String>) {
        let text_win: WINDOW = derwin(self.status_window, self.status_size[0] - 2, self.status_size[1] - 2, 1, 1);
        
        for (line, string) in status.iter().enumerate() {
            mvwprintw(text_win, line as i32, 1, string.as_str());
        }
        wrefresh(text_win);
    }

    pub fn update_output(&self, c: u8) {
        waddch(self.output_buffer, c as u32);
        wrefresh(self.output_buffer);
    }

    pub fn write_log(&self, log: String) {
        waddstr(self.logging_buffer, log.as_str());
    }

    pub fn get_input(&self) -> i32 {
        let input = wgetch(self.output_buffer);
        
        
        if input == ERR {

            return 0x00;
        }

        self.write_log(format!("{}\n", input));
        return input;
    }
}