use super::memory::Memory;

mod addr_modes;
mod arithmatic;
mod branch;
mod control;
mod flags;
mod helpers;
mod increment;
mod load_store;
mod logic;
mod shift;
mod stack;
mod transfer;
mod ins_decoding;
// mod super::memory; hoe reference ik een super module;

type Byte = u8;
type Word = u16;

pub struct Processor {
    pub pc: Word,       // Program Counter
    pub sp: Byte,       // Stack pointer

    pub a: Byte, 
    pub x: Byte,
    pub y: Byte,

    pub stat: Byte
    //7  bit  0
    // ---- ----
    // NVss DIZC
    // |||| ||||
    // |||| |||+- Carry
    // |||| ||+-- Zero
    // |||| |+--- Interrupt Disable
    // |||| +---- Decimal
    // ||++------ No CPU effect, see: the B flag
    // |+-------- Overflow
    // +--------- Negative
}

impl Processor {
    pub fn new() -> Processor {
        return Processor {
            pc: 0xFF00,
            sp: 0x00FF,
            
            a: 0,
            x: 0,
            y: 0,
            
            stat: 0,
        }
    }

    pub fn execute(&mut self, memory: &mut Memory) {
        let opcode: u8 = memory.read(self.pc);
        self.pc = self.pc.wrapping_add(1);
        let ins_page: u8 = opcode & 0b11;
        let self_mode: u8 = (opcode & 0b00011100) >> 2;
        let instruction:u8 = (opcode & 0b11100000) >> 5;

        let single_bytes: u8 = self.single_bytes(opcode, memory);

        if single_bytes == 1 {
            let data: u16 = self.loader(ins_page, self_mode, memory);

            self.runner(ins_page, instruction, memory, data);
        }
    }

    fn single_bytes(&mut self, opcode: u8, memory: &mut Memory) -> u8 {
        match opcode {
            0xEA => { return 0 }, //NOOP
            0x08 => { self.php(memory) }, //PHP
            0x28 => { self.plp(memory) }, // PLP
            0x48 => { self.pha(memory) }, //PHA
            0x68 => { self.pla(memory) }, //PLA
            0x88 => { self.dey() }, //DEY
            0xCA => { self.dex() }, //DEX
            0xA8 => { self.tay() }, //TAY
            0xC8 => { self.iny() }, //INY
            0xE8 => { self.inx() }, //INX
            0x18 => { self.clc() }, //CLC
            0x38 => { self.sec() }, //SEC
            0x58 => { self.cli() }, //CLI
            0x78 => { self.sei() }, //SEI
            0x98 => { self.tya() }, //TYA
            0xB8 => { self.clv() }, //CLV
            0xD8 => { self.cld() }, //CLD
            0xF8 => { self.sed() }, //SED
            0x8A => { self.txa() }, //TXA
            0x9A => { self.txs() }, //TXS
            0xAA => { self.tax() }, //TAX
            0xBA => { self.tsx() }, //TSX
            0x10 => { self.bpl(memory) }, //BPL
            0x30 => { self.bmi(memory) }, //BMI
            0x50 => { self.bvc(memory) }, //BVC
            0x70 => { self.bvs(memory) }, //BVS
            0x90 => { self.bcc(memory) }, //BCC
            0xB0 => { self.bcs(memory) }, //BCS
            0xD0 => { self.bne(memory) }, //BNE
            0xF0 => { self.beq(memory) }, //BEQ
            0x20 => { self.jsr(memory) }, //JSR
            0x60 => { self.rts(memory) }, //RTS
            _    => { return 1 }
        }
        return 0
    }

    fn loader(&mut self, ins_page: u8, ld_mode: u8, memory: & mut Memory) -> u16 {
        let mode: addr_modes::AddrMode = addr_modes::AddrMode::decode(ins_page, ld_mode);
        let data: u16 = mode.get_addr(self, memory);
        return data;
    }

    fn runner(&mut self, ins_page: u8, instruction: u8, memory: &mut Memory, address: u16) {
        match ins_page {
            0b01 => {
                match instruction {
                    0b000 => { self.ora(memory, address) },
                    0b001 => { self.and(memory, address) },
                    0b010 => { self.eor(memory, address) },
                    0b011 => { self.adc(memory, address) },
                    0b100 => { self.sta(memory, address) },
                    0b101 => { self.lda(memory, address) },
                    0b110 => { self.cmp(memory, address) },
                    0b111 => { self.sbc(memory, address) },
                    _     => { return }
                }
            },
            0b10 => {
                match instruction {
                    0b000 => { self.asl(memory, address) },
                    0b001 => { self.rol(memory, address) },
                    0b010 => { self.lsr(memory, address) },
                    0b011 => { self.ror(memory, address) },
                    0b100 => { self.stx(memory, address) },
                    0b101 => { self.ldx(memory, address) },
                    0b110 => { self.dec(memory, address) },
                    0b111 => { self.inc(memory, address) },
                    _     => { return }
                }
            },
            0b00 => {
                match instruction {
                    0b001 => { self.bit(memory, address) },
                    0b010 => { self.jmp(address) },
                    0b011 => { self.jmp(address) }, // jump abs
                    0b100 => { self.sty(memory, address) },
                    0b101 => { self.ldy(memory, address) },
                    0b110 => { self.cpy(memory, address) },
                    0b111 => { self.cpx(memory, address) },
                    _     => { return }
                }
            },
            _   => { return }
        }
    }
}



