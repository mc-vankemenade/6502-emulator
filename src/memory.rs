
use std::io::prelude::*;
use std::fs::File;

pub struct Memory {
    ram: Ram,
    io: [u8; 64],
    rom: Rom
}

impl Memory {
    pub fn new(rom: &str) -> Memory {
        return Memory { ram: Ram::new(), io: [0; 64], rom: Rom::from_file(rom) }
    }

    pub fn read(&mut self, address: u16) -> u8 {
        match address {
            0x0000..=0x8000 => {
                let address: u16 = address;
                return self.ram.read(address);
            },
            0xD010..=0xD013 => {
                let address_mapped: u16 = address - 0xD010;

                // If the keyboard or screen register is read. Set its corresponding status register to 0;
                if address == 0xD010 {
                    self.write(0xD011, 0);
                }

                return self.io[address_mapped as usize];
            },
            0xFF00..=0xFFFF => {
                let address: u16 = address - 0xFF00;
                return self.rom.read(address);

            },
            _ => {
                //println!("Outside Memory: {}", address);
                return 0xEA
            }
        }
    }

    pub fn write(&mut self, address: u16, data: u8) {
        match address {
            0x0000..=0x8000 => {
                let address: u16 = address;
                self.ram.write(address, data);
                return;
            },
            0xD010..=0xD013 => {
                let address: u16 = address - 0xD010;
                self.io[address as usize] = data;
                return;
            },
            0xFF00..=0xFFFF => {
                let address: u16 = address - 0xFF00;
                self.rom.write(address, data);
                return;

            },
            _ => {
                //println!("Outside Memory: {}", address);
                return;
            }
        }
    }
}

struct Rom {
    pub bytes: [u8; 256]    
}

impl Rom {
    pub fn from_file(filename: &str,) -> Rom {
        let file = File::open(filename);
        let mut buffer = [0; 256];

        if file.is_ok() {
            let _ = file.unwrap().read(&mut buffer);

            return Rom {
                bytes: buffer
            }
        }
        return Rom { bytes: [0; 256] }
    }

    pub fn read(&self, address: u16) -> u8 {
        return self.bytes[address as usize];
    }

    pub fn write(&mut self, address: u16, data:u8) {
        self.bytes[address as usize] = data;
    }
}

struct Ram {
    pub bytes: [u8; 1024*48]
}

impl Ram {
    fn new() -> Ram {
        return Ram {
            bytes: [0; 1024*48]
        }
    }
    pub fn read(&self, address: u16) -> u8 {
        return self.bytes[address as usize];
    }
    pub fn write(&mut self, address: u16, data:u8) {
        self.bytes[address as usize] = data;
    }
}