mod cpu;
mod memory;
mod emutui;

use std::thread;
use std::time::{Instant, Duration};

use std::sync::mpsc;

struct Computer {
    key_in: mpsc::Sender<u8>,
    scr_out: mpsc::Receiver<u8>,
    pause: mpsc::Sender<bool>,
    status_out: mpsc::Receiver<Vec<String>>,

    comp_handle: thread::JoinHandle<()>,

    tui: emutui::Emutui
    
}

impl Computer {
    pub fn new() -> Computer{
        let (key_in, key_out) = mpsc::channel();
        let (scr_in, scr_out) = mpsc::channel();
        let (pause_in, pause_out) = mpsc::channel();
        let (status_in, status_out) = mpsc::channel();


        let comp_handle = thread::spawn(move || {
            let mut processor = cpu::Processor::new();
            let mut memory = memory::Memory::new("bin/wozmon.bin");
            // pause thread beforehand.
            let mut pause = true;

            loop {
                // if pause is true stay in top of the loop
                if pause == true {
                    pause = pause_out.recv().unwrap();
                }

                // Lock execution of 1 cycle to 1MHz
                let now = Instant::now();
                processor.execute(&mut memory);
                let duration = now.elapsed().as_nanos();
                if duration < 1000 {
                    thread::sleep(Duration::from_nanos((1000 - duration) as u64));
                }
                

                // Try to read input channel, if Ok write tot memory
                let input = key_out.try_recv();
                if input.is_ok() {
                    memory.write(0xD010, input.unwrap());
                    memory.write(0xD011, 0b10000000);
                }

                // Read character from memory if !0x00 send to main thread
                let output = memory.read(0xD012);
                if output != 0x00 {
                    let _ = scr_in.send(output);
                }
                memory.write(0xD012, 0x00);

                let status_msg: Vec<String> = vec![
                    format!("Program Counter: {:#04x}", processor.pc),
                    format!("Stack Pointer: {:#04x}", processor.sp),
                    format!(""),
                    format!("Registers:"),
                    format!(" - A: {:#04x}", processor.a),
                    format!(" - X: {:#04x}", processor.x),
                    format!(" - Y: {:#04x}", processor.y),
                    format!(""),
                    format!("                   NVssDIZC"),
                    format!("Status Register: {:#010b}", processor.stat),
                    //format!("keyboard register: {:#04x}", memory::io)
                ];

                let _ = status_in.send(status_msg);

                let new_pause = pause_out.try_recv();
                if new_pause.is_ok() {
                    pause = new_pause.unwrap();
                }

            }
        });

        return Computer {
            key_in: key_in,
            scr_out: scr_out,
            pause: pause_in,
            status_out: status_out,
            comp_handle: comp_handle,
            tui: emutui::Emutui::new()
        }
    }
    pub fn run(&mut self) {
        loop {
            let now = Instant::now();
            // Get input keys and send to processor
            let input = self.tui.get_input();
            if input != 0x00 && input <= 255  && input != 91 && input != 93{
                let _ = self.key_in.send(input as u8);
            }

            match input as u32 {
                91 => {
                    let _ = self.pause.send(true);
                }
                93 => {
                    let _ = self.pause.send(false);
                }
                _ => {}
            }

            // Receive output from processor
            let output = self.scr_out.try_recv();
            if output.is_ok() {
                self.tui.update_output(output.unwrap())
            }

            // Receive status from processor
            let status = self.status_out.try_recv();
            if status.is_ok() {
                self.tui.update_status(status.unwrap());
            }
            let duration = now.elapsed().as_nanos();
            if duration < 1000 {
                thread::sleep(Duration::from_nanos((1000 - duration) as u64));
            }
        }
    }
}

fn main() {

    let mut comp: Computer = Computer::new();
    comp.run();
    // main thread moet chillen

}   

