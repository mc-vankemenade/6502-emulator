use super::Processor;
use super::Memory;

impl Processor {
    pub fn read_word(&mut self, address: u16, memory: & mut Memory) -> u16 {
        let mut data: u16 = memory.read(address) as u16;
    
        data |= (memory.read(address + 1) as u16) << 8;
    
        return data;
    }

    pub fn status_negative(&mut self, result: u8) {
        if (result as i8) < 0 {
            self.stat = self.stat | 0b10000000
        } else {
            self.stat = self.stat & 0b01111111
        }
    }

    pub fn status_carry(&mut self, result: u16) {
        if result > 255 {
            self.stat = self.stat | 0b00000001
        } else {
            self.stat = self.stat & 0b11111110
        }
    }

    pub fn status_overflow(&mut self, result: i16) {
        if result > 127 || result < -127 {
            self.stat = self.stat | 0b01000000
        } else {
            self.stat = self.stat & 0b10111111
        }
    }

    pub fn status_zero(&mut self, result: u8) {
        if result == 0 {
            self.stat = self.stat | 0b00000010
        } else {
            self.stat = self.stat & 0b11111101
        }
    }
}