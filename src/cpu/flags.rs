use super::Processor;

impl Processor {
    pub fn clc(&mut self) {
        self.stat = self.stat & 0b11111110;
    }

    pub fn cld(&mut self) {
        self.stat = self.stat & 0b11110111;
    }

    pub fn cli(&mut self) {
        self.stat = self.stat & 0b11111011;
    }

    pub fn clv(&mut self) {
        self.stat = self.stat & 0b10111111;
    }

    pub fn sec(&mut self) {
        self.stat = self.stat | 0b00000001;
    }

    pub fn sed(&mut self) {
        self.stat = self.stat | 0b00001000;
    }

    pub fn sei(&mut self) {
        self.stat = self.stat | 0b00000100;    
    }
}