use super::Processor;
use super::Memory;

impl Processor {
    pub fn and(&mut self, memory: &mut Memory, address: u16) {
        let data = memory.read(address);
        self.a = self.a & data as u8;

        self.status_negative(self.a);
        self.status_zero(self.a);
    }

    pub fn bit(&mut self, memory: &mut Memory, address: u16) {
        let result: u16 = memory.read(address) as u16 & self.a as u16;

        self.status_negative(result as u8);
        self.status_overflow(result as i16); // TODO
        self.status_zero(result as u8);
    }

    pub fn eor(&mut self, memory: &mut Memory, address: u16) {
        let data = memory.read(address);
        self.a = self.a ^ data as u8;

        self.status_negative(self.a);
        self.status_zero(self.a);
    }
    
    pub fn ora(&mut self, memory: &mut Memory, address: u16) {
        let data = memory.read(address);
        self.a = self.a | data as u8;

        self.status_negative(self.a);
        self.status_zero(self.a);
    }
}