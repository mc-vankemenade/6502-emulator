use super::Processor;
use super::Memory;

impl Processor {
    pub fn jsr(&mut self, memory: & mut Memory) {
        let dest: u16 = self.read_word(self.pc, memory);
        self.pc = self.pc.wrapping_add(2);

        self.sp = self.sp.wrapping_sub(1);
        memory.write(self.sp as u16, (self.pc >> 8) as u8);
        self.sp = self.sp.wrapping_sub(1);
        memory.write(self.sp as u16, self.pc as u8);

        self.pc = dest;
    }

    pub fn rts(&mut self, memory: & mut Memory) {
        let dest: u16 = self.read_word(self.sp as u16, memory);

        self.sp = self.sp.wrapping_add(2);
        self.pc = dest;
    }

    pub fn rti(&mut self, memory: & mut Memory) {
        self.stat = memory.read(self.sp as u16);
        self.sp = self.sp.wrapping_add(1);
        
        let dest: u16 = self.read_word(self.sp as u16, memory);

        self.sp = self.sp.wrapping_add(2);
        self.pc = dest;
    }

    pub fn jmp(&mut self, address: u16) {
        self.pc = address;
    }
}