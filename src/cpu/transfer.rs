use super::Processor;

impl Processor {
    pub fn tay(&mut self) {
        self.y = self.a;

        self.status_negative(self.y);
        self.status_zero(self.y);
    }

    pub fn tya(&mut self) {
        self.a = self.y;

        self.status_negative(self.a);
        self.status_zero(self.a);
    }

    pub fn txa(&mut self) {
        self.a = self.x;
        self.status_negative(self.a);
        self.status_zero(self.a);
    }

    pub fn txs(&mut self) {
        self.sp = self.x;
    }

    pub fn tax(&mut self) {
        self.x = self.a;

        self.status_negative(self.x);
        self.status_zero(self.x);
    }

    pub fn tsx(&mut self) {
        self.x = self.sp;
        self.status_negative(self.x);
        self.status_zero(self.x);
    }
}

#[cfg(test)]
mod tests {
    use crate::cpu::Processor;

    #[test]
    fn tay_test() {
        let mut processor = Processor::new();
        processor.a = 254;
        processor.y = 1;
        processor.tay();

        assert!(processor.a == processor.y);
        assert!((processor.stat & 0b10000000) == 0b10000000);

        processor.a = 0;
        processor.y = 1;
        processor.tay();

        assert!(processor.a == processor.y);
        assert!((processor.stat & 0b00000010) == 0b00000010);
    }

    #[test]
    fn tax_test() {
        let mut processor = Processor::new();
        processor.a = 254;
        processor.x = 1;
        processor.tax();

        assert!(processor.a == processor.x);
        assert!((processor.stat & 0b10000000) == 0b10000000);

        processor.a = 0;
        processor.x = 1;
        processor.tax();

        assert!(processor.a == processor.x);
        assert!((processor.stat & 0b00000010) == 0b00000010);
    }
}