use super::Processor;
use super::Memory;

impl Processor {
    pub fn inc(&mut self, memory: &mut Memory, address: u16) {
        let data = memory.read(address).wrapping_add(1);
        memory.write(address, data);

        self.status_negative(data);
        self.status_zero(data);
    }

    pub fn inx(&mut self) {
        self.x = self.x.wrapping_add(1);

        self.status_negative(self.x);
        self.status_zero(self.x);
    }

    pub fn iny(&mut self) {
        self.y = self.y.wrapping_add(1);

        self.status_negative(self.y);
        self.status_zero(self.y);
    }

    pub fn dec(&mut self, memory: &mut Memory, address: u16) {
        let data = memory.read(address).wrapping_sub(1);
        memory.write(address, data);

        self.status_negative(data);
        self.status_zero(data);
    }

    pub fn dex(&mut self) {
        self.x = self.x.wrapping_sub(1);

        self.status_negative(self.x);
        self.status_zero(self.x);
    }

    pub fn dey(&mut self) {
        self.y = self.y.wrapping_sub(1);

        self.status_negative(self.y);
        self.status_zero(self.y);
    }
}