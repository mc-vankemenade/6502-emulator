use super::Processor;
use super::Memory;

impl Processor {
    pub fn php(&mut self, memory: & mut Memory) {
        memory.write(self.sp as u16, self.a);
        self.sp = self.sp.wrapping_sub(1);
    }

    pub fn plp(&mut self, memory: & mut Memory) {
        self.stat = memory.read(self.sp as u16);
        self.sp = self.sp.wrapping_add(1);
    }

    pub fn pha(&mut self, memory: & mut Memory) {
        memory.write(self.sp as u16, self.a);
        self.sp = self.sp.wrapping_sub(1);
    }

    pub fn pla(&mut self, memory: & mut Memory) {
        self.a = memory.read(self.sp as u16);
        self.sp = self.sp.wrapping_add(1);

        self.status_negative(self.a);
        self.status_zero(self.a);
    }
}