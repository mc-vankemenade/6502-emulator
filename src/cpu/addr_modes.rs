use core::panic;

use super::Processor;
use crate::memory::Memory;

pub enum AddrMode {
    Immediate,
    Relative,
    Absolute,
    AbsoluteX,
    AbsoluteY,
    ZeroPage,
    ZeroPageX,
    ZeroPageY,
    Implied
}

impl AddrMode {
    pub fn decode(page: u8, ins: u8) -> AddrMode {
        match page {
            0b01 => {
                match ins {
                    0b000 => { AddrMode::ZeroPageX },
                    0b001 => { AddrMode::ZeroPage },
                    0b010 => { AddrMode::Immediate },
                    0b011 => { AddrMode::Absolute },
                    0b100 => { AddrMode::ZeroPageY },
                    0b101 => { AddrMode::ZeroPageX },
                    0b110 => { AddrMode::AbsoluteY },
                    0b111 => { AddrMode::AbsoluteX },
                    _     => { panic!("invalid adressing mode!") }
                }
            },

            0b10 => {
                match ins {
                    0b000 => { AddrMode::Immediate },
                    0b001 => { AddrMode::ZeroPage },
                    0b010 => { AddrMode::Implied },
                    0b011 => { AddrMode::Absolute },
                    0b101 => { AddrMode::ZeroPageX },
                    0b111 => { AddrMode::AbsoluteX },
                    _     => { panic!("invalid adressing mode!") }
                }
            },

            0b00 => {
                match ins {
                    0b000 => { AddrMode::Immediate },
                    0b001 => { AddrMode::ZeroPage },
                    0b011 => { AddrMode::Absolute },
                    0b101 => { AddrMode::ZeroPageX },
                    0b111 => { AddrMode::AbsoluteX },
                    _     => { panic!("invalid adressing mode!") }
                }
            }

            _ => {
                panic!("invalid Adressing mode page")
            }

        }
    }


    pub fn get_addr(&self, cpu: &mut Processor, mem: &mut Memory) -> u16 {
        match self {
            AddrMode::Immediate => {
                let data: u16 = cpu.pc;
                cpu.pc = cpu.pc.wrapping_add(1);
                return data;
            }

            AddrMode::Relative => {
                return 0;
            }

            AddrMode::Absolute => {
                let data: u16 = cpu.read_word(cpu.pc, mem);
                cpu.pc = cpu.pc.wrapping_add(2);
                return data;
            }

            AddrMode::AbsoluteX => {
                let data: u16 = cpu.read_word(cpu.pc, mem);
                cpu.pc = cpu.pc.wrapping_add(2);
                return data + cpu.x as u16;
            }

            AddrMode::AbsoluteY => {
                let data: u16 = cpu.read_word(cpu.pc, mem);
                cpu.pc = cpu.pc.wrapping_add(2);
                return data + cpu.y as u16;
            }

            AddrMode::ZeroPage => {
                let data: u8 = mem.read(cpu.pc);
                cpu.pc = cpu.pc.wrapping_add(1);
                return data as u16;
            }

            AddrMode::ZeroPageX => {
                let data: u8 = mem.read(cpu.pc);
                cpu.pc = cpu.pc.wrapping_add(1);
                return (data + cpu.x) as u16;
            }

            AddrMode::ZeroPageY => {
                let data: u8 = mem.read(cpu.pc);
                cpu.pc = cpu.pc.wrapping_add(1);
                return (data + cpu.x) as u16;
            }

            _ => {
                return 0;
            }
        }
    }
}