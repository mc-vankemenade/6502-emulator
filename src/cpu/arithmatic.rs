use super::Processor;
use super::Memory;

impl Processor {
    pub fn adc(&mut self, memory: &mut Memory, address: u16) {
        let data = memory.read(address);

        let addit: u16 = self.a as u16 + data as u16 + (self.stat & 0b00000001) as u16;
        self.a = addit as u8;
        self.status_carry(addit);
        self.status_negative(self.a);
        self.status_zero(self.a);
    }
    pub fn cmp(&mut self, memory: &mut Memory, address: u16) {
        let data = memory.read(address);
        let result = self.a as i8 - data as i8;

        if self.a > data {
            self.stat &= 0b11111100;
            if result < 0 {
                self.stat |= 0b10000000;
            } else {
                self.stat &= 0b01111111;
            }
            return
        }

        if self.a == data {
            self.stat |= 0b00000011;
            return
        }

        if self.a < data {
            self.stat |= 0b00000001;
            self.stat &= 0b11111101;
            if result < 0 {
                self.stat |= 0b10000000;
            } else {
                self.stat &= 0b01111111;
            }
        }
    }
    pub fn cpx(&mut self, memory: &mut Memory, address: u16) {
        let data = memory.read(address);
        let result = self.x as i8 - data as i8;

        if self.x > data {
            self.stat &= 0b11111100;
            if result < 0 {
                self.stat |= 0b10000000;
            } else {
                self.stat &= 0b01111111;
            }
            return
        }

        if self.x == data {
            self.stat |= 0b00000011;
            return
        }

        if self.x < data {
            self.stat |= 0b00000001;
            self.stat &= 0b11111101;
            if result < 0 {
                self.stat |= 0b10000000;
            } else {
                self.stat &= 0b01111111;
            }
        }
    }
    pub fn cpy(&mut self, memory: &mut Memory, address: u16) {
        let data = memory.read(address);
        let result = self.y as i8 - data as i8;

        if self.y > data {
            self.stat &= 0b11111100;
            if result < 0 {
                self.stat |= 0b10000000;
            } else {
                self.stat &= 0b01111111;
            }
            return
        }

        if self.y == data {
            self.stat |= 0b00000011;
            return
        }

        if self.y < data {
            self.stat |= 0b00000001;
            self.stat &= 0b11111101;
            if result < 0 {
                self.stat |= 0b10000000;
            } else {
                self.stat &= 0b01111111;
            }
        }
    }

    pub fn sbc(&mut self, memory: &mut Memory, address: u16) {
        // SBC
        let result: u16 = self.a as u16 - memory.read(address) as u16;
        if result as i16 >= 0 {
            self.stat |= 0b00000001;
        } else {
            self.stat &= 0b11111110;
        }
        self.status_overflow(result as i16);
        self.status_negative(result as u8);
        self.status_zero(result as u8);
    }
}