use super::Processor;
use super::Memory;

impl Processor {
    pub fn bpl(&mut self, memory: & mut Memory) {
        if (self.stat | 0b01111111) == 0b01111111 {
            let next: i8 = memory.read(self.pc) as i8;
            self.pc = self.pc.wrapping_add(1);
            self.pc = self.pc.wrapping_add(next as u16);
        } else {
            self.pc = self.pc.wrapping_add(1);
        }
    }

    pub fn bmi(&mut self, memory: & mut Memory) {
        if (self.stat & 0b10000000) == 0b10000000 {
            let next: i8 = memory.read(self.pc) as i8;
            self.pc = self.pc.wrapping_add(1);
            self.pc = self.pc.wrapping_add(next as u16);
        } else {
            self.pc = self.pc.wrapping_add(1);
        }
    }

    pub fn bvc(&mut self, memory: & mut Memory) {
        if (self.stat | 0b10111111) == 0b10111111 {
            let next: i8 = memory.read(self.pc) as i8;
            self.pc = self.pc.wrapping_add(1);
            self.pc = self.pc.wrapping_add(next as u16);
        } else {
            self.pc = self.pc.wrapping_add(1);
        }
    }

    pub fn bvs(&mut self, memory: & mut Memory) {
        if (self.stat & 0b01000000) == 0b01000000 {
            let next: i8 = memory.read(self.pc) as i8;
            self.pc = self.pc.wrapping_add(1);
            self.pc = self.pc.wrapping_add(next as u16);
        } else {
            self.pc = self.pc.wrapping_add(1);
        }
    }

    pub fn bcc(&mut self, memory: & mut Memory) {
        if (self.stat | 0b11111110) == 0b11111110 {
            let next: i8 = memory.read(self.pc) as i8;
            self.pc = self.pc.wrapping_add(1);
            self.pc = self.pc.wrapping_add(next as u16);
        } else {
            self.pc = self.pc.wrapping_add(1);
        }
    }

    pub fn bcs(&mut self, memory: & mut Memory) {
        if (self.stat & 0b0000001) == 0b00000001 {
            let next: i8 = memory.read(self.pc) as i8;
            self.pc = self.pc.wrapping_add(1);
            self.pc = self.pc.wrapping_add(next as u16);
        } else {
            self.pc = self.pc.wrapping_add(1);
        }
    }

    pub fn bne(&mut self, memory: & mut Memory) {
        if (self.stat | 0b11111101) == 0b11111101 {
            let next: i8 = memory.read(self.pc) as i8;
            self.pc = self.pc.wrapping_add(1);
            self.pc = self.pc.wrapping_add(next as u16);
        } else {
            self.pc = self.pc.wrapping_add(1);
        }
    }

    pub fn beq(&mut self, memory: & mut Memory) {
        if (self.stat & 0b00000010) == 0b00000010 {
            let next: i8 = memory.read(self.pc) as i8;
            self.pc = self.pc.wrapping_add(1);
            self.pc = self.pc.wrapping_add(next as u16);
        } else {
            self.pc = self.pc.wrapping_add(1);
        }
    }
}