use super::Processor;
use super::Memory;

impl Processor {
    pub fn asl(&mut self, memory: &mut Memory, address: u16) {
        // ASL
        // Memory dependant en single byte instructie
        let asl: u16;
        if address == 0xFFFF {
            // Push bit 7 into carry flag
            if self.a & 0b10000000 > 0 {
                self.stat = self.stat | 0b00000001;
            } else {
                self.stat = self.stat & 0b11111110;
            }
            
            // shift accumulator
            asl = (self.a as u16) << 1;
            self.a = asl as u8;
        } else {
            // Push bit 7 into carry flag
            if memory.read(address) & 0b10000000 > 0 {
                self.stat = self.stat | 0b00000001;
            } else {
                self.stat = self.stat & 0b11111110;
            }

            // shift memory
            asl = (memory.read(address) as u16) << 1;
            memory.write(address, asl as u8);
        }
        self.status_zero(asl as u8);
        self.status_negative(asl as u8);
    }

    pub fn lsr(&mut self, memory: &mut Memory, address: u16) {
        let asl: u8;
        if address == 0xFFFF {
            // Push bit 1 into carry flag
            if self.a & 0b00000001 > 0 {
                self.stat = self.stat | 0b00000001;
            } else {
                self.stat = self.stat & 0b11111110;
            }

            // shift accumulator
            asl = self.a >> 1;
            self.a = asl;
        } else {
            // Push bit 1 into carry flag
            if self.a & 0b00000001 > 0 {
                self.stat = self.stat | 0b00000001;
            } else {
                self.stat = self.stat & 0b11111110;
            }

            asl = memory.read(address) >> 1;
            memory.write(address, asl);
        }
        self.status_zero(asl);
        self.status_negative(asl);
    }

    pub fn rol(&mut self, memory: &mut Memory, address: u16) {
        // ROL
        let rol: u8;
        let old_carry: u8;
        let new_carry: u8;

        if address == 0xFFFF {
            old_carry = if self.stat & 0b00000001 > 0 {0b00000001} else {0b00000000};
            new_carry = if self.a & 0b10000000 > 0 {0b00000001} else {0b00000000};

            // rotate accumulator
            rol = self.a << 1;
            self.a = rol | old_carry;

            // update carry
            self.stat = self.stat | new_carry;
        } else {

            old_carry = if self.stat & 0b00000001 > 0 {0b00000001} else {0b00000000};
            new_carry = if memory.read(address) & 0b10000000 > 0 {0b00000001} else {0b00000000};

            // rotate accumulator
            rol = memory.read(address) << 1;
            memory.write(address, rol | old_carry);

            // update carry
            self.stat = self.stat | new_carry;
        }
        self.status_zero(rol as u8);
        self.status_negative(rol as u8);
    }

    pub fn ror(&mut self, memory: &mut Memory, address: u16) {
        // ROL
        let rol: u8;
        let old_carry: u8;
        let new_carry: u8;

        if address == 0xFFFF {
            old_carry = if self.stat & 0b00000001 > 0 {0b10000000} else {0b00000000};
            new_carry = if self.a & 0b00000001 > 0 {0b00000001} else {0b00000000};

            // rotate accumulator
            rol = self.a >> 1;
            self.a = rol | old_carry;

            // update carry
            self.stat = self.stat | new_carry;
        } else {

            old_carry = if self.stat & 0b00000001 > 0 {0b00000001} else {0b00000000};
            new_carry = if memory.read(address) & 0b10000000 > 0 {0b00000001} else {0b00000000};

            // rotate accumulator
            rol = memory.read(address) >> 1;
            memory.write(address, rol | old_carry);

            // update carry
            self.stat = self.stat | new_carry;
        }
        self.status_zero(rol as u8);
        self.status_negative(rol as u8);
    }
}