use super::Processor;
use super::Memory;

impl Processor {
    pub fn lda(&mut self, memory: &mut Memory, address: u16) {
        self.a = memory.read(address);

        self.status_negative(self.a);
        self.status_zero(self.a);
    }

    pub fn ldx(&mut self, memory: &mut Memory, address: u16) {
        self.x = memory.read(address);

        self.status_negative(self.x);
        self.status_zero(self.x);
    }

    pub fn ldy(&mut self, memory: &mut Memory, address: u16) {
        self.y = memory.read(address);

        self.status_negative(self.y);
        self.status_zero(self.y);
    }

    pub fn sta(&mut self, memory: &mut Memory, address: u16) {
        memory.write(address, self.a);
    }

    pub fn stx(&mut self, memory: &mut Memory, address: u16) {
        memory.write(address, self.x);
    }

    pub fn sty(&mut self, memory: &mut Memory, address: u16) {
        memory.write(address, self.y);
    }
}