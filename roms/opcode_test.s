.org $FF00

BCC_TEST:
    LDA #$01
    CMP #$00
    BCC BCS_TEST
    JSR PRINT_FAIL
    JMP BREAK

BCS_TEST:
    JSR PRINT_OK
    LDA #$00
    CMP #$01
    BCS BEQ_TEST
    JSR PRINT_FAIL
    JMP BREAK
    
BEQ_TEST:
    JSR PRINT_OK
    LDA #$10
    CMP #$10
    BEQ BNE_TEST
    JSR PRINT_FAIL
    JMP BREAK   

BNE_TEST:
    JSR PRINT_OK
    LDA #$11
    CMP #$10
    BNE RESET
    JSR PRINT_FAIL
    JMP BREAK
    
RESET:
    JSR PRINT_OK
    JMP $FF00

PRINT_OK:
    LDA #"O"
    STA $D012
    LDA #"K"
    STA $D012
    LDA #$0A        ; carriage return
    STA $D012
    RTS

PRINT_FAIL:
    LDA #"F"
    STA $D012
    LDA #"A"
    STA $D012
    LDA #"I"
    STA $D012
    LDA #"L"
    STA $D012
    LDA #$0A        ; carriage return
    STA $D012
    RTS

BREAK:
    JMP BREAK